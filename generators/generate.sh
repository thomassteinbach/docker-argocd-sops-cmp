#!/usr/bin/env bash

# This script first looks for all SOPS encrypted files and decrypts them in place.
# Second it generates the Kubernetes manifests depending on the repository type (Helm, kustomize, ...)
#
# Configuration:
#  * DEBUG:      When set to 'true', then the script is executed with 'set -x' and the output is logged to
#                '${DEBUG_PATH}/generate.log'. Is unset by default.
#  * DEBUG_PATH: The path to the log files when 'DEBUG=true' is set. Defaults to "/tmp/xarif".

# error/exit on any failure
set -e

PARAM_DEBUG_PATH=${PARAM_DEBUG_PATH:-"/tmp/xarif"}

if [ "$PARAM_DEBUG" = true ]; then
  mkdir -p "$PARAM_DEBUG_PATH"
  # tee a script from within the script
  # https://superuser.com/a/1104136
  exec &> >(tee "${PARAM_DEBUG_PATH}/generate.log")
  set -x
fi

SCRIPT_PATH="$(readlink -f $0 | xargs dirname)"

# The working directory of this script is something like
# '/tmp/_cmp_server/<hash>' plus the path configured in the
# ArgoCD application. We want to identify the root of the repository,
# without the path configured in the application. This is the first
# three sections of the working director '/tmp/_cmp_server/<hash>':
REPOSITORY_ROOT=$(echo $PWD | awk -F'/' '{print "/" $2 "/" $3 "/" $4}')

# decrypt all SOPS encrypted files
for file in $(find ${REPOSITORY_ROOT} -type f); do
  if sops filestatus $file 2>/dev/null | grep -q '"encrypted":true'; then
    if [ "$PARAM_IGNORE_SOPS_ERRORS" = true ]; then
      sops --decrypt --in-place $file || true
    else
      sops --decrypt --in-place $file
    fi
  fi
done

if test -f "Chart.yaml"; then
    exec env "$SCRIPT_PATH/generate-helm.sh"
elif test -f "kustomization.yaml"; then
    exec env "$SCRIPT_PATH/generate-kustomize.sh"
else
    exec env "$SCRIPT_PATH/generate-plain.sh"
fi
