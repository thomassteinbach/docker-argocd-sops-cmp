#!/usr/bin/env bash
#
# Configuration:
#  * DEBUG:      When set to 'true', then the script is executed with 'set -x' and the output is logged to
#                '${DEBUG_PATH}/generate-kustomize.log'. Is unset by default.
#  * DEBUG_PATH: The path to the log files when 'DEBUG=true' is set. Defaults to "/tmp/xarif".

# error/exit on any failure
set -e

PARAM_DEBUG_PATH=${PARAM_DEBUG_PATH:-"/tmp/xarif"}

if [ "$PARAM_DEBUG" = true ]; then
  mkdir -p "$PARAM_DEBUG_PATH"
  # tee a script from within the script
  # https://superuser.com/a/1104136
  exec &> >(tee "${PARAM_DEBUG_PATH}/generate-kustomize.log")
  set -x
fi

HELM_FLAGS=""

if [[ "$KUBE_VERSION" ]]; then
  HELM_FLAGS="$HELM_FLAGS --helm-kube-version=${KUBE_VERSION}"
fi

if [[ "${KUBE_API_VERSIONS}" ]]; then
    HELM_FLAGS="${HELM_FLAGS} --helm-api-versions=${KUBE_API_VERSIONS}"
fi

kustomize build --load-restrictor=LoadRestrictionsNone --enable-helm ${HELM_FLAGS} .