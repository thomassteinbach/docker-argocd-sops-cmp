#!/usr/bin/env bash

#
# Configuration:
#  * DEBUG:      When set to 'true', then the script is executed with 'set -x' and the output is logged to
#                '${DEBUG_PATH}/generate-plain.log'. Is unset by default.
#  * DEBUG_PATH: The path to the log files when 'DEBUG=true' is set. Defaults to "/tmp/xarif".

# error/exit on any failure
set -e

PARAM_DEBUG_PATH=${PARAM_DEBUG_PATH:-"/tmp/xarif"}

if [ "$PARAM_DEBUG" = true ]; then
  mkdir -p "$PARAM_DEBUG_PATH"
  # tee a script from within the script
  # https://superuser.com/a/1104136
  exec &> >(tee "${PARAM_DEBUG_PATH}/generate-plain.log")
  set -x
fi

find . -type f \( -name "*.yml" -o -name "*.yaml" \) -exec sh -c 'echo "---"; cat {}; echo' \;