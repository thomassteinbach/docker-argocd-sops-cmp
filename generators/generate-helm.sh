#!/usr/bin/env bash

## Following variables in this script are coming from ArgoCD
## https://argoproj.github.io/argo-cd/user-guide/build-environment/
# ARGOCD_APP_NAME - name of application
# ARGOCD_APP_NAMESPACE - destination application namespace.
# KUBE_VERSION="<major>.<minor>"
# KUBE_API_VERSIONS="v1,apps/v1,..."
#
# Configuration:
#  * DEBUG:      When set to 'true', then the script is executed with 'set -x' and the output is logged to
#                '${DEBUG_PATH}/generate-helm.log'. Is unset by default.
#  * DEBUG_PATH: The path to the log files when 'DEBUG=true' is set. Defaults to "/tmp/xarif".

# error/exit on any failure
set -e

PARAM_DEBUG_PATH=${PARAM_DEBUG_PATH:-"/tmp/xarif"}

if [ "$PARAM_DEBUG" = true ]; then
  mkdir -p "$PARAM_DEBUG_PATH"
  # tee a script from within the script
  # https://superuser.com/a/1104136
  exec &> >(tee "${PARAM_DEBUG_PATH}/generate-helm.log")
  set -x
fi

HELM=$(which helm)

# Collect Helm options from
# `helm template --help`

if [ "$PARAM_IGNORE_CRDS" = true ]; then
    HELM_FLAGS=""
else
    HELM_FLAGS="--include-crds"
fi


if [[ "$ARGOCD_APP_NAMESPACE" ]]; then
    HELM_FLAGS="${HELM_FLAGS} --namespace=${ARGOCD_APP_NAMESPACE}"
fi

if [[ "$KUBE_VERSION" ]]; then
  HELM_FLAGS="$HELM_FLAGS --kube-version=${KUBE_VERSION}"
fi

if [[ "${KUBE_API_VERSIONS}" ]]; then
    HELM_API_VERSIONS=""
    for v in ${KUBE_API_VERSIONS//,/ }; do
        HELM_API_VERSIONS="${HELM_API_VERSIONS} --api-versions=$v"
    done
    HELM_FLAGS="${HELM_FLAGS} ${HELM_API_VERSIONS}"
fi

#
# Custom Configuration of the Plugin over environment variables
#

if [[ "${PARAM_HELM_VALUES_FILES}" ]]; then
    INTERNAL_HELM_VALUES_FILES=""
    for v in ${PARAM_HELM_VALUES_FILES//,/ }; do
        INTERNAL_HELM_VALUES_FILES="${INTERNAL_HELM_VALUES_FILES} --values=$v"
    done
    HELM_FLAGS="${HELM_FLAGS} ${INTERNAL_HELM_VALUES_FILES}"
fi

if [[ "${PARAM_HELM_VALUES}" ]]; then
    helm_values_file=$(mktemp /tmp/argocdvalues.XXXXXXX)
    echo -e "${PARAM_HELM_VALUES}" > "$helm_values_file"
    HELM_FLAGS="${HELM_FLAGS} --values=$helm_values_file"
fi

if [[ "${PARAM_HELM_VALUES_BASE64}" ]]; then
    helm_values_file_b64=$(mktemp /tmp/argocdvaluesb64.XXXXXX)
    echo "${PARAM_HELM_VALUES_BASE64}" | base64 -d | sed 's/\\n/\n/g' > "$helm_values_file_b64"
    HELM_FLAGS="${HELM_FLAGS} --values=$helm_values_file_b64"
fi

INTERNAL_HELM_RELEASE_NAME="$ARGOCD_APP_NAME"
if [[ "${PARAM_HELM_RELEASE_NAME}" ]]; then
    INTERNAL_HELM_RELEASE_NAME="$PARAM_HELM_RELEASE_NAME"
fi

# Execute Helm
${HELM} template ${INTERNAL_HELM_RELEASE_NAME} . ${HELM_FLAGS}

if [[ "${PARAM_HELM_VALUES}" ]]; then
  rm "$helm_values_file"
fi

if [[ "${PARAM_HELM_VALUES_BASE64}" ]]; then
  rm "$helm_values_file_b64"
fi
