# ArgoCD Config Management Plugin with SOPS support

This Docker image contains an ArgoCD plugin for decrypting Mozilla SOPS encrypted files.
This is done by instructing ArgoCD to run the `./generators/generate.sh` script for creating Kubernetes manifests.
This script is searching for all SOPS encrypted files in the ArgoCD Applications repository and decrypt them in place.
Then depending on the project type (Helm, Kustomize, plain Kubernetes manifests), the Kubernetes manifests will be
generated and handed over to ArgoCD on stdout.

## Activating the plugin in ArgoCD

The config management plugin Docker container must be added as sidecar container to the ArgoCD deployment.
Using the ArgoCD Helm chart, this could be done by following chart configuration:

```yaml
repoServer:
  extraContainers:
    - name: argocd-sops-cmp
      command:
        # Entrypoint should be Argo CD lightweight CMP server i.e. argocd-cmp-server
        - /var/run/argocd/argocd-cmp-server
      image: registry.gitlab.com/thomassteinbach/docker-argocd-sops-cmp/main:latest
      securityContext:
        runAsNonRoot: true
        runAsUser: 999
      # Optional - for using sops with age
      env:
        # Mount the age key file for sops within the plugin image
        # https://github.com/getsops/sops#encrypting-using-age
        - name: SOPS_AGE_KEY_FILE
          value: /var/run/secrets/sops/keys.txt
      volumeMounts:
        - mountPath: /var/run/argocd
          name: var-files
        - mountPath: /home/argocd/cmp-server/plugins
          name: plugins
        # Starting with v2.4, do NOT mount the same tmp volume as the repo-server container. The filesystem separation helps
        # mitigate path traversal attacks.
        - mountPath: /tmp
          name: argocd-sops-cmp-tmp
        # Optional - when using sops with age
        - name: sops-age-key-file
          mountPath: /var/run/secrets/sops/
  volumes:
    - name: argocd-sops-cmp-tmp
      emptyDir: {}
    # Optional: - when using sops with age
    - name: sops-age-key-file
      secret:
        secretName: argocd-sops-age-key-file
```

Note: Your cluster/the ArgoCD pod must have the permission, to use the KMS key for decrypting files.

* Read more information about ArgoCD plugins in the official docs:</br>
  <https://argo-cd.readthedocs.io/en/stable/operator-manual/config-management-plugins/>

## Using this plugin for your Application

For using this plugin you have to

* omit the source project type
* provide the `spec.source.plugin.name`

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: guestbook
  namespace: argocd
spec:
  project: default
  source:
    repoURL: https://github.com/argoproj/argocd-example-apps.git
    targetRevision: HEAD
    path: guestbook
    plugin:
      name: argocd-sops-cmp
```

This plugin works with repositories containing Helm charts, Kustomize deployments and plain Kubernetes manifests.
If your repository isn't a Helm repo, identified by a `Chart.yaml`, or a Kustomize repo, identified by a `kustomization.yaml`,
then it would be assumed as a repository containing plain Kubernetes manifestes.
Latter means that all files ending with `.yml` or `.yaml` (also recursively in subfolders) will be passed to ArgoCD.

Any YAML, JSON, ENV, INI and BINARY files in your Kubernetes deployment repository can be SOPS encrypted.
The plugin will automatically decrypt all encrypted files before handing them over to ArgoCD. There is no
need to follow any conventions of which files can be encrypted.

The detection of SOPS encrypted files is by searching for the RegEx `'mac: ENC\['` in them.
This string is contained in all SOPS encrypted files.
In the rare case a non-SOPS file also contains this string, this plugin must be extended for more flexibility.

* We have an example repository containing all three supported types of deployments:</br>
  <https://git.tech.rz.db.de/serviceteamdbv2/eks-basic-resources/argo-sops-examples>
* For more information about Mozilla SOPS please refer to the official documentation:</br>
  <https://github.com/mozilla/sops>

## Configuration of Helm Deployments

This plugin is configured by environment variables. Following variables are currently supported for Helm:

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
...
spec:
  source:
    plugin:
      name: argocd-sops-cmp
      parameters:
        # Helm values files for overriding values in the helm chart.
        # The path is relative to the spec.source.path directory defined above.
        # Either a single value or a comma separated list.
        - name: helm-values-files
          array:
            - common/values.yaml
            - dev/values.yaml
        # Optional: A YAML string overwriting/extending existing value configuration.
        # WARNING: If your value contains shell variables, that should not be expanded
        # by ArgoCD OR your values contain characters that could be interpreted by a
        # shell, then use HELM_VALUES_BASE64 instead!
        - name: helm-values
          string: |
            foo:
              bar: baz
        # Optional: Like 'HELM_VALUES' but base64 encoded. This approach is more safe
        # then HELM_VALUES but a little bit more complicated.
        - name: helm-values-base64
          string: eyJmb28iOiB7ImJhciI6ICJiYXoifX0K
        # Optional setting, if no Custom Resource Definitions should be deployed
        - name: helm-ignore-crds
          string: "false"
```

## Debugging

Activate debugging by setting the configuration parameter `debug=true`:

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
...
spec:
  source:
    plugin:
      name: argocd-sops-cmp
      parameters:
        # If 'true', then the whole scipt execution will be logged under '/tmp/xarif' in the repo
        - name: debug
          string: "false"
        # Override the log path, when 'debug' is 'true'.
        - name: debug-path
          string: /tmp/xarif
```

Then detailed output of the script execution will be written to log files
within the pod `argocd-repo-server-*` to `/tmp/xarif`. This path can be modified by the
parameter `debug-path`.

## Special configuration

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
...
spec:
  source:
    plugin:
      name: argocd-sops-cmp
      parameters:
        # If 'true', errors returned when executing SOPS won't lead to a failed deployment.
        # Useful when having multiple secrets for multiple environments, where one environment
        # cannot decrypt the files of another environment.
        - name: ignore-sops-errors
          string: "false"
```
