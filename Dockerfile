FROM alpine:3 as dependencies

#
# binary version configurations
#
ARG SOPS_VERSION="v3.9.4"
ARG KUSTOMIZE_VERSION="v5.4.1"
ARG HELM_VERSION="v3.14.4"

#
# build the image
#

ENV BINARY_INSTALL_DIR=/usr/local/sbin

# hadolint ignore=DL3018
RUN apk --no-cache update && \
    apk --no-cache add curl tar && \
    mkdir -p ${BINARY_INSTALL_DIR}

# download SOPS binary
RUN curl https://github.com/getsops/sops/releases/download/${SOPS_VERSION}/sops-${SOPS_VERSION}.linux.amd64 \
    -L -o ${BINARY_INSTALL_DIR}/sops && \
    chmod +x ${BINARY_INSTALL_DIR}/sops

# download Kustomize binary
RUN curl -L https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz | \
    tar -xz -C ${BINARY_INSTALL_DIR}/ && \
    chmod +x ${BINARY_INSTALL_DIR}/kustomize

# download Helm binary
RUN curl -L https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | \
    tar -xz -C /tmp/ && \
    cp /tmp/linux-amd64/helm ${BINARY_INSTALL_DIR}/helm && \
    chmod +x ${BINARY_INSTALL_DIR}/helm

FROM alpine:3 as cmp

ENV BINARY_INSTALL_DIR=/usr/local/sbin

# hadolint ignore=DL3018
RUN apk --no-cache update && \
    apk --no-cache add bash && \
    mkdir -p ${BINARY_INSTALL_DIR}

COPY --from=dependencies ${BINARY_INSTALL_DIR}/sops ${BINARY_INSTALL_DIR}/sops
COPY --from=dependencies ${BINARY_INSTALL_DIR}/kustomize ${BINARY_INSTALL_DIR}/kustomize
COPY --from=dependencies ${BINARY_INSTALL_DIR}/helm ${BINARY_INSTALL_DIR}/helm

COPY generators /generators

# https://argo-cd.readthedocs.io/en/stable/operator-manual/config-management-plugins/#place-the-plugin-configuration-file-in-the-sidecar
WORKDIR /home/argocd/cmp-server/config/
COPY --chown=999:999 plugin.yaml ./

# https://argo-cd.readthedocs.io/en/stable/operator-manual/config-management-plugins/#place-the-plugin-configuration-file-in-the-sidecar
# "Make sure that sidecar container is running as user 999"
USER 999
